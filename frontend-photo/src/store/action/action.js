import axios from '../../axios-api';
import { NotificationManager} from 'react-notifications';
import {push} from 'connected-react-router'
export const REGISTR_USER_SUCCESS = 'REGISTR_USER_SUCCESS';
export const REGISTR_USER_FAILURE = 'REGISTR_USER_FAILURE ';


export const DELETE_PHOTO_SUCCESS = 'DELETE_PHOTO_SUCCESS';


export const LOGIN_USER_SUCCESS = 'LOGIN_USER_SUCCESS';
export const LOGIN_USER_FAILURE = 'LOGIN_USER_FAILURE';

export const LOGOUT_USER = 'LOGOUT_USER';

export const FETCH_PHOTO_SUCCESS = 'FETCH_PHOTO_SUCCESS';

export const CREATE_PHOTO_SUCCESS = 'CREATE_PHOTO_SUCCESS';


export const fetchPhotoSuccess = (photos) => ({type: FETCH_PHOTO_SUCCESS, photos})

export const createPhotoSuccess = () => ({type: CREATE_PHOTO_SUCCESS});
export const registerUserSuccess = (user) => ({type: REGISTR_USER_SUCCESS, user});

export const registerUserFailure = error => ({type: REGISTR_USER_FAILURE, error});

export const loginUserSuccess = user => ({type: LOGIN_USER_SUCCESS, user});
export const loginUserFailure = error => ({type: LOGIN_USER_SUCCESS, error});

export const deletePhotoSuccess = () => ({type: DELETE_PHOTO_SUCCESS});

export const logoutUser = () => {
    return (dispatch, getState )=> {
        const token  = getState().reducer.user.token;
        const config = {headers: {'Authorization': token}};
        return axios.delete('/users/sessions', config).then(
            () => {
                dispatch({type: LOGOUT_USER});
                dispatch(fetchPhoto());
                NotificationManager.success('Logged out!');
            },
            error => {
                NotificationManager.error('Could not logout')
            }
        )
    }
};



export const registerUser = userData => {
    return dispatch => {
        return axios.post('/users', userData).then(
            response => {
                dispatch(registerUserSuccess(response.data.user));
                NotificationManager.success('Registered successfully');
                dispatch(push('/'))
            },
            error => {
                if (error.response && error.response.data) {
                    dispatch(registerUserFailure(error.response.data))
                } else {
                    dispatch(registerUserFailure({global: 'No connection'}))
                }
            }
        )
    }
};

export const loginUser = userData => {
    return dispatch => {
        return axios.post('/users/sessions', userData).then(
            response => {
                dispatch(loginUserSuccess(response.data.user));
                NotificationManager.success('Logged in successfully!');
                dispatch(push('/'))
            },
            error => {
                if (error.response && error.response.data) {
                    dispatch(loginUserFailure(error.response.data))
                } else {
                    dispatch(loginUserFailure({global: 'No connect'}))
                }
            }
        )
    }


};

export const facebookLogin = userData => {
    return dispatch => {
        return axios.post('/users/facebookLogin', userData).then(
            response => {
                dispatch(loginUserSuccess(response.data.user));
                NotificationManager.success('Logged in via Facebook');
                dispatch(push('/'))
            },
            () => {
                dispatch(loginUserSuccess('Login via Facebook failed'))
            }
        )
    }
};

export const fetchPhoto = (id) => {
    let url = "/photos";
    if (id){
        url += `?user=${id}`
    }
    return dispatch => {
        axios.get(url).then(
            response => {
                dispatch(fetchPhotoSuccess(response.data));
            }
        )
    }
};


export const createPhoto = (photoData) => {
    return dispatch => {
        axios.post('/photos', photoData).then(
            () => {
                    dispatch(createPhotoSuccess());
                    dispatch(push('/'))
            }
        )
    }
};



export const deletePhoto = (photoId, userId) => {
    return dispatch => {
        axios.delete('/photos/' + photoId).then(
            () => dispatch(deletePhotoSuccess()),
                    dispatch(fetchPhoto(userId))
        )
    }
};