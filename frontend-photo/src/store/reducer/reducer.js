import {
    FETCH_PHOTO_SUCCESS,
    LOGIN_USER_FAILURE,
    LOGIN_USER_SUCCESS, LOGOUT_USER,
    REGISTR_USER_FAILURE,
    REGISTR_USER_SUCCESS
} from "../action/action";

const initialState = {
    registerError: null,
    loginError: null,
    user: null,
    photos: [],
    photo: null
};

const usersReducer = (state = initialState, action) => {
    switch (action.type) {
        case REGISTR_USER_SUCCESS:
            return {...state, registerError: null, user: action.user};
        case REGISTR_USER_FAILURE:
            return {...state, registerError: action.error};
        case LOGIN_USER_SUCCESS:
            return {...state, user: action.user, loginError: null};
        case LOGIN_USER_FAILURE:
            return {...state, loginErrorL: action.error};
        case LOGOUT_USER:
            return {...state, user: null };
        case FETCH_PHOTO_SUCCESS:
            return {...state, photos: action.photos};
        default:
            return state
    }
};

export default usersReducer;