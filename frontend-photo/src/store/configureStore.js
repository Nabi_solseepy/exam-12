import {connectRouter, routerMiddleware} from "connected-react-router";
import thunkMiddleware from "redux-thunk";

import axios from '../axios-api';

import {applyMiddleware, combineReducers, compose, createStore} from "redux";
import {createBrowserHistory} from "history";
import reducer from "./reducer/reducer";
export const history = createBrowserHistory();

const rootReducer = combineReducers({
    router: connectRouter(history),
    reducer: reducer
});


const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

const middleware = [
    thunkMiddleware,
    routerMiddleware(history)
];



const enhancers = composeEnhancers(applyMiddleware(...middleware));


const store = createStore( rootReducer,enhancers);

axios.interceptors.request.use(config => {
    try {
        config.headers['Authorization'] = store.getState().reducer.user.token
    } catch (e) {
        // do nothing user is
    }
    return config
});



export default store;