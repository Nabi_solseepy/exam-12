import React, {Fragment} from 'react';
import { DropdownItem, DropdownMenu, DropdownToggle, NavLink, UncontrolledDropdown} from "reactstrap";
import {NavLink as RouterNavLink} from "react-router-dom";

const UserMenu = ({user, logout}) => (
    <Fragment>
        <UncontrolledDropdown nav inNavbar>


            <DropdownToggle  nav caret>
                Hello, {user.username}
            </DropdownToggle>
            <DropdownMenu right>
                <NavLink tag={RouterNavLink} to={'/UserPhoto/' + user._id}>

                <DropdownItem>
                  my photo
                </DropdownItem>
        </NavLink>

            <DropdownItem>

                </DropdownItem>

                <DropdownItem divider />
                <DropdownItem onClick={logout}>
                    Logout
                </DropdownItem>
            </DropdownMenu>
        </UncontrolledDropdown>
    </Fragment>

);




export default UserMenu;