import React, {Fragment} from 'react';
import './Modal.css'

import Backdrop from '../Backdrop/Backdrop'
import {Button} from "reactstrap";

const Modal = props => {
    return (
        <Fragment>
            <Backdrop show={props.show} close={props.close}/>
            <div
                className="Modal"
                style={
                    {transform: props.show ? 'translateY(0)':  'translateY(-100vh)',
                        opacity: props.show ? '1' : 0
                    }
                }>
                {props.children}

                <div>
                    <Button onClick={props.close}>close</Button>
                </div>
            </div>
        </Fragment>
    );
};

export default Modal;