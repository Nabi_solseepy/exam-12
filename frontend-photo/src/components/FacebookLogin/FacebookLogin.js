import React, {Component} from 'react';
import FacebookLoginButton from "react-facebook-login/dist/facebook-login-render-props";
import {Button} from "reactstrap";
import {connect} from "react-redux";
import {facebookLogin} from "../../store/action/action";
import {NotificationManager} from 'react-notifications'

class FacebookLogin extends Component {

    facebookLogin = data => {
        console.log(data);
        if (data.error){
            NotificationManager.error('Something went wrong!');
            this.props.facebookLogin(data)
        } else if (!data.name) {
            NotificationManager.error('You pressed cancel');
        } else {
            this.props.facebookLogin(data);
        }
    };
    render() {
        return (
            <div>
                <FacebookLoginButton
                    appId="1487849131350701"
                    callback={this.facebookLogin}
                    fields="name,email,picture"
                    render={renderProps => (
                        <Button onClick={renderProps.onClick} color="primary">Login with Facebook</Button>
                    )}
                />
            </div>
        );
    }
}

const mapDispatchToProps = dispatch => ({
    facebookLogin: userData => dispatch(facebookLogin(userData))
});

export default connect(null,mapDispatchToProps)(FacebookLogin);