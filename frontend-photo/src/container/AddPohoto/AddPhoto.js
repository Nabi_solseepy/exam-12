import React, {Component,} from 'react';
import {Button, Col, Form, FormGroup, Input, Label} from "reactstrap";
import {connect} from "react-redux";
import {createPhoto} from "../../store/action/action";

class AddPhoto extends Component {

    state = {
        title: '',
        image: ''
    };


    inputChangeHandler = event => {
        this.setState({
            [event.target.name]: event.target.value
        })
    };

    fileChangeHandler = event => {
        this.setState({
            [event.target.name]: event.target.files[0]
        });
    };

    submitFormHandler = event => {
        event.preventDefault();

        const formData = new FormData();


        Object.keys(this.state).forEach(key => {

                formData.append(key, this.state[key]);

        });

        this.props.createPhoto(formData);
    };

    render() {
        return (
         <Form onSubmit={this.submitFormHandler}>
             <FormGroup row>
                 <Label for="exampleEmail" sm={2}>Title</Label>
                 <Col sm={10}>
                     <Input type="text" name="title" placeholder="Title" onChange={this.inputChangeHandler} />
                 </Col>
             </FormGroup>
             <FormGroup row>
                 <Label for="examplePassword" sm={2}>Image</Label>
                 <Col sm={10}>
                     <Input  type="file" name="image"   placeholder="image" onChange={this.fileChangeHandler}/>
                 </Col>
             </FormGroup>
             <FormGroup row>
                 <Col sm={{offset: 2, size: 10}}>
                     <Button type="submit" color="primary">
                         Add image
                     </Button>
                 </Col>
             </FormGroup>
         </Form>
        );
    }
}

const mapStateToProps = state => ({
    photos: state.reducer.photos
});


const mapDispatchToProps = dispatch => ({
    createPhoto: (photoData) => dispatch(createPhoto(photoData))
});

export default connect(mapStateToProps, mapDispatchToProps)(AddPhoto);