import React, {Component} from 'react';
import {Button, Col, Form, FormGroup, Input, Label,} from "reactstrap";
import {connect} from "react-redux";
import {registerUser} from "../../store/action/action";
import FacebookLogin from "../../components/FacebookLogin/FacebookLogin";

class Register extends Component {
    state = {
        username: '',
        password: ''
    };


    inputChangeHandler = event => {
        this.setState({
            [event.target.name]: event.target.value
        })
    };

    submitFormHandler = event => {
        event.preventDefault();

        this.props.registerUser({...this.state})
    };


    render() {
        return (
            <Form onSubmit={this.submitFormHandler}>
                <FormGroup row>
                    <Label for="exampleEmail" sm={2}>Email</Label>
                    <Col sm={10}>
                        <Input type="text" name="username" id="exampleEmail" placeholder="with a placeholder" onChange={this.inputChangeHandler} />
                    </Col>
                </FormGroup>
                <FormGroup row>
                    <Label for="examplePassword" sm={2}>Password</Label>
                    <Col sm={10}>
                        <Input type="password" name="password" id="examplePassword" placeholder="password placeholder" onChange={this.inputChangeHandler}/>
                    </Col>
                </FormGroup>
                <FormGroup row>
                    <Col sm={{offset: 2, size: 10}}>
                        <Button type="submit" color="primary">
                            Register
                        </Button>
                    </Col>
                </FormGroup>
                <FacebookLogin/>
            </Form>
        );
    }
}

const mapStateToProps = state => ({
    error: state.reducer.registerError
});

const mapDispatchToProps = dispatch => ({
    registerUser: (userData) => dispatch(registerUser(userData))
});


export default connect(mapStateToProps, mapDispatchToProps)(Register);