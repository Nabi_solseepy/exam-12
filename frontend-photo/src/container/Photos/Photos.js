import React, {Component, Fragment} from 'react';
import {Card, CardBody, CardColumns, CardImg, CardTitle} from "reactstrap";
import {connect} from "react-redux";
import {fetchPhoto} from "../../store/action/action";
import {Link} from "react-router-dom";
import Modal from "../../components/UI/Modal/Modal";

class Photos extends Component {

state ={
    show: false,
    photo: null
};

toggle = (photo) => {
    // this.props.fetchPhotoId(id);
    this.setState({
        show: true,
        photo: photo
    })
};

 close = () => {
     this.setState({
         show: false,
         photo: null
     })
 };

    componentDidMount() {
        this.props.fetchPhoto();
    }
    render() {
        return (
            <Fragment>
                <CardColumns>
                {this.props.photos.map(photo => (
                    <Card  key={photo._id}>
                        <CardImg  top width="100%" src={"http://localhost:8000/uploads/" + photo.image} alt="Card image cap" onClick={ () => this.toggle(photo)} />
                        <CardBody>
                            <Link to={'/UserPhoto/' + photo.user._id} >
                            <CardTitle>{photo.user.username}</CardTitle>
                            <CardTitle>{photo.title}</CardTitle>
                            </Link>
                        </CardBody>
                    </Card>
                ))}
                </CardColumns>
                <Modal  show={this.state.show} close={this.close}>
                    {this.state.photo && <CardImg top width="100%" src={"http://localhost:8000/uploads/" + this.state.photo.image} alt="Card image cap" />}

                </Modal>
            </Fragment>

        );
    }
}


const mapStateToProps = state => ({
    photos: state.reducer.photos,
    photo: state.reducer.photo
});

const mapDispatchToProps = dispatch => ({
    fetchPhoto: () => dispatch(fetchPhoto()),
});

export default connect(mapStateToProps, mapDispatchToProps)(Photos);