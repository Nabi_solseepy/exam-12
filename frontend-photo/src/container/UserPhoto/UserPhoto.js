import React, {Component, Fragment} from 'react';
import {Button, Card, CardBody, CardColumns, CardImg, CardTitle} from "reactstrap";
import {deletePhoto, fetchPhoto} from "../../store/action/action";
import {connect} from "react-redux";
import Modal from "../../components/UI/Modal/Modal";

class UserPhoto extends Component {
    state ={
        show: false,
        photo: null
    };

    toggle = (photo) => {
        this.setState({
            show: true,
            photo: photo
        })
    };

    close = () => {
        this.setState({
            show: false,
        })
    };

    componentDidMount() {
        this.props.fetchPhoto(this.props.match.params.id)

    }

    createPhoto = () => {
        this.props.history.push('/add_photo')
    };

    render() {
        return (
            <Fragment>
                <h3 className="pt-3 pb-3">
                    {this.props.user && this.props.user.username}
                    { this.props.user &&  (
                        this.props.user._id === this.props.match.params.id? <Button onClick={this.createPhoto} className="float-right">Add new Photo</Button>: null
                    )}
                </h3>
                <CardColumns>


                    {this.props.photos.map(photo => (
                        <Card key={photo._id} >
                            <CardImg onClick={ () => this.toggle(photo)}  top width="100%" src={"http://localhost:8000/uploads/" + photo.image} alt="Card image cap" />
                            <CardBody>
                                <CardTitle>{photo.user.username}</CardTitle>
                                    <CardTitle>{photo.title}</CardTitle>
                                {this.props.user && this.props.user._id === photo.user._id ?   <Button onClick={() => this.props.deletePhoto(photo._id, photo.user._id)} color="danger">Delete</Button>: null}

                            </CardBody>

                        </Card>
                    ))}

                    <Modal  show={this.state.show} close={this.close}>
                        {this.state.photo && <CardImg top width="100%" src={"http://localhost:8000/uploads/" + this.state.photo.image} alt="Card image cap" />}

                    </Modal>


                </CardColumns>


            </Fragment>

        );
    }
}

const mapStateToProps = state => ({
    photos: state.reducer.photos,
    user: state.reducer.user
});

const mapDispatchToProps = dispatch => ({
    fetchPhoto: (id) => dispatch(fetchPhoto(id)),
    deletePhoto: (id, userId) => dispatch(deletePhoto(id, userId))
});

export default connect(mapStateToProps, mapDispatchToProps)(UserPhoto);