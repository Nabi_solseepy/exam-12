import React, {Component,Fragment} from 'react';
import './App.css';
import Toolbar from "./components/UI/Toolbar/Toolbar";
import {connect} from "react-redux";
import Register from "./container/Register/Register";
import Login from "./container/Login/Login";
import {Route} from "react-router-dom";
import {Container} from "reactstrap";
import {logoutUser} from "./store/action/action";
import {NotificationContainer} from "react-notifications";
import Photos from "./container/Photos/Photos";
import UserPhoto from "./container/UserPhoto/UserPhoto";
import AddPhoto from "./container/AddPohoto/AddPhoto";


class App extends Component {
    render() {
        return (

            <Fragment>
                <NotificationContainer/>
                <Toolbar user={this.props.user}
                         logout={this.props.logoutUser}
                />
                <Container>
                       <Route path="/" exact component={Photos} />
                        <Route path="/register" exact component={Register} />
                        <Route path="/login" exact component={Login} />
                        <Route path="/UserPhoto/:id" exact component={UserPhoto}/>
                        <Route path='/add_photo' exact component={AddPhoto}/>
                </Container>
            </Fragment>
        );
    }
}

const mapStateTpProps = state => ({
    user: state.reducer.user
});


const  mapDispatchToProps= dispatch => ({
    logoutUser: () => dispatch(logoutUser())
});
export default connect(mapStateTpProps,mapDispatchToProps)(App);
