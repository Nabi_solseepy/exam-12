const express = require('express');
const nanoid = require('nanoid');
const config = require('../config');
const path = require('path');
const Photo = require('../models/Photo');

const tryAuth = require('../meddleware/tryAuth')

const auth = require('../meddleware/auth');
const multer = require('multer');


const storage = multer.diskStorage({
    destination: (req, file, cd) => {
        cd(null, config.uploadPath);
    },
    filename: (req, file, cd) => {
        cd(null, nanoid() + path.extname(file.originalname))
    }
});

const upload = multer({storage});

const router = express.Router();


router.post('/', auth, upload.single('image'), async (req,res ) => {

    const photoData = req.body;

    if (req.file) {
        photoData.image = req.file.filename;
    }

    const  photo = new Photo(photoData);

     photo.user = req.user._id;

    photo.save()
        .then(result => res.send(result))
        .catch(error => res.status(400).send(error));
});




router.get('/:id', (req, res) => {


    Photo.findById(req.params.id)
        .then(photo => {
            if (photo) res.send(photo);
            else res.sendStatus(404);
        })
        .catch(() => res.sendStatus(500))
});



router.get('/', tryAuth, async (req,res) => {

    const criteria  = {};

    if (req.query.user){
        criteria.user = req.query.user
    }
    Photo.find(criteria).populate({path: 'user', select: 'username'})
        .then(photo => res.send(photo) )
        .catch(() => res.sendStatus(500))
});



router.delete('/:id', auth, async (req, res) => {

    try {
        const photo = await Photo.findByIdAndDelete({_id: req.params.id});
        if (!photo){
            return res.sendStatus(404)
        }

        await photo.save();

        res.status(200).send(photo)
    } catch (e) {
        res.send(e)
    }

});







module.exports = router;