const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const PhotosSchema = new Schema({
    user: {
        type: Schema.Types.ObjectId,
        ref: 'User',
        required: true
    },
    title: {
        type: String
    },
    image: {
        type: String
    }
});

const Photo = mongoose.model('Photo', PhotosSchema);

module.exports = Photo;