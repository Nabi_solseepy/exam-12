const path = require('path');

const rootPath = __dirname;

module.exports = {
    rootPath,
    uploadPath: path.join(rootPath, 'public/uploads'),
    dbUrl:'mongodb://localhost/photos',
    mongoOptions: {
        useNewUrlParser: true,
        useCreateIndex: true
    },
    facebook: {
        appId: '1487849131350701',
        appSecret: '5b6c6b5761194f93db6418356c99bea8' // insecure!
    }
};
