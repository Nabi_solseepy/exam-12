const mongoose = require('mongoose');
const nanoid = require('nanoid');

const config = require('./config');

const User = require('./models/User');

const Photo = require('./models/Photo');

const run = async () => {
    await mongoose.connect(config.dbUrl, config.mongoOptions);

    const connection = mongoose.connection;

    const collections = await connection.db.collections();

    for (let collection of collections) {
        await collection.drop();
    }

  const user = await  User.create(
        {
            username: 'Alex Kim',
            password: '123',
            token: nanoid()
        },
        {
            username: 'John Doe',
            password: '123',
            token: nanoid()
        }
    );

    await Photo.create(
    {
        user: user[0]._id,
        title: 'anime',
        image: 'yter.jpg'
    },
    {
        user: user[1]._id,
        title: 'nbrhood',
        image: 'com.jpeg'
    }
    );

    return connection.close();
};


run().catch(error => {
    console.error('Something wrong happened...', error);
});
